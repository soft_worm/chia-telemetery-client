#!/bin/bash

cd ~

sudo apt update
sudo apt upgrade -y
sudo apt install git software-properties-common iotop htop -y

git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git ~/ecopool-client

sudo apt install curl libcurl4 -y
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install nodejs -y

cd ~/ecopool-client
sudo npm i -g pm2

pm2 delete all

sudo npm i -g yarn

yarn install

DEBUG=ecopool-client:* pm2 start npm --name api -- run start
DEBUG=ecopool-client:* pm2 start npm --name frontend -- run serve
pm2 save

echo ""
echo ""
echo ""
echo ""
echo "================================================================================="
echo "Ecopool client will be available on url http://localhost:3401/ and http://$(hostname -I):3401 in several moments"
echo "Клиент ecopool будет запущен по адресу http://localhost:3401/ и http://$(hostname -I):3401 через несколько мгновений"
echo "================================================================================="
