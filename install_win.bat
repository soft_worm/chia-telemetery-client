@echo off


git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git C:\ecopool-client
curl https://nodejs.org/dist/v12.13.0/node-v12.13.0-x64.msi --output C:\node-v12.13.0-x64.msi
C:\node-v12.13.0-x64.msi
cd C:\ecopool-client

SET DEBUG=ecopool-client:*

CMD /C npm i -g pm2
CMD /C pm2 delete all

CMD /C npm i -g yarn

CMD /C yarn install

CMD /C pm2 start "C:\Program Files\nodejs\node.exe" --name api -- ./backend/bin/www
CMD /C pm2 start .\node_modules\@vue\cli-service\bin\vue-cli-service.js --name frontend -- serve
CMD /C pm2 save

echo ""
echo ""
echo ""
echo ""
echo "================================================================================="
echo "Ecopool client will be available on url http://localhost:3401/ and http://$(hostname -I):3401 in several moments"
echo "Клиент ecopool будет запущен по адресу http://localhost:3401/ и http://$(hostname -I):3401 через несколько мгновений"
echo "================================================================================="
echo "Press any key to continue"
pause > nul