module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/scss/variables/index.scss";',
      }
    }
  },
  devServer: {
    port: '3401'
  },
}
