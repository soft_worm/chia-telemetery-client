import packageSrc from '../../../package.json';

const state = () => ({
    // token: localStorage.getItem('chia-client-token') || '',
    pathStatus: localStorage.getItem('chia-client-path') || false,
    isAuth: false,
    newVersion: packageSrc.version,
    userBalance: 0,
    pendingWithdrawals: []
});

const getters = {
    isAuth: state => !!state.token,
    isPathStatus: state => {
        return state.pathStatus === 'true' || state.pathStatus === true;
    },
};

const mutations = {
    setPathStatus: (state, status) => {
        state.pathStatus = status;
        localStorage.setItem('chia-client-path', status);
    },
    setAuth:(state, newVal) => {
        state.isAuth = newVal;
    },
    setNewVersion: (state, newVal) => {
        state.newVersion = newVal;
    },
    setBalance: (state, newVal) => {
        state.userBalance = newVal;
    },
    setPendingWithdrawals: (state, newVal) => {
        state.pendingWithdrawals = newVal;
    }
};

const actions = {};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}
