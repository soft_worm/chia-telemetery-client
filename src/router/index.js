import { createRouter, createWebHistory } from 'vue-router'
import { store } from '../store';

// unauth
import RegistrationPage from '/src/pages/Registration';
import LoginPage from '/src/pages/Login';

// auth
import DashboardPage from '/src/pages/Dashboard';
import WalletPage from '/src/pages/Wallet';
import FarmPage from '/src/pages/Farm';
import RequestChiaPathPage from '/src/pages/RequestChiaPathPage';

function isAuth (to, from, next) {
  switch(true) {
    case store.state.user.isAuth && store.getters['user/isPathStatus']:
      return next();
    case store.state.user.isAuth && !store.getters['user/isPathStatus']:
      return next('/request_chia_path');
    default:
      return next('/login');
  }
}

function isSetPath (to, from, next) {
  if (store.state.user.isAuth && !store.getters['user/isPathStatus']) {
    return next()
  }
  store.commit('user/logout');
  return next('/login');
}

function isUnAuth (to, from, next) {
  if (!store.state.user.isAuth) {
    return next()
  }
  return next('/dashboard');
}

const routes = [
  {
    path: '/registration',
    name: 'Registration',
    component: RegistrationPage,
    beforeEnter: isUnAuth
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginPage,
    beforeEnter: isUnAuth
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: DashboardPage,
    beforeEnter: isAuth
  },
  {
    path: '/wallet',
    name: 'Wallet',
    component: WalletPage,
    beforeEnter: isAuth
  },
  {
    path: '/farm',
    name: 'Farm',
    component: FarmPage,
    beforeEnter: isAuth,
  },
  {
    path: '/request_chia_path',
    name: 'RequestChiaPath',
    component: RequestChiaPathPage,
    beforeEnter: isSetPath,
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/login',
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router