import { createApp } from 'vue';
import { store } from './store';
import App from './App.vue';
import router from './router';

import byteSize from 'byte-size';

import ElLoading from 'element-plus/lib/el-loading';

byteSize.defaultOptions({
    toString() {
        return `${this.value} ${this.unit}`
    }
});

import 'element-plus/lib/theme-chalk/index.css';
import './assets/scss/index.scss';

const app = createApp(App);

app.use(store);
app.use(router);
app.use(ElLoading);

app.mount('#app');
