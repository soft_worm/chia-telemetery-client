const createError = require('http-errors');
const cors = require('cors')
const config = require('config');
// const middleware = require('./middleware');
const userRegistrationRoute = require('./user/registration');
const userLoginRoute = require('./user/login');
const userLogoutRoute = require('./user/logout');
const userRoutes = require('./user');
const harvesterRoutes = require('./harvester');
const updateClient = require('./update-client');

const ROUTES_ERROR_ANSWERS = require('../constants/RoutesErrorAnswers');

const morgan = require('morgan');

module.exports = (app) => {
  app.use(morgan('combined'));

  app.use(cors({
    origin(origin, setOrigin) {
      setOrigin(null, origin);
      return origin;
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 200
  }));

  app.use('/check-login', (req, res) => {
    res.json(!!config.currentUser);
  });

  app.use('/registration', userRegistrationRoute);
  app.use('/login', userLoginRoute);
  app.use('/logout', userLogoutRoute);
  app.use('/user', userRoutes);
  app.use('/harvester', harvesterRoutes);
  app.use('/update-client', updateClient);

  app.use((req, res, next) => {
    return next(createError(404, `${ROUTES_ERROR_ANSWERS[404]}: ${req.url}`));
  });

  app.use((error, req, res) => {
    res.locals.message = error.message;
    res.locals.error = req.app.get('env') === 'development' ? error : {};

    const status = error.status || 500;

    res.status(status);
    res.send(ROUTES_ERROR_ANSWERS[status]);
  });
}
