const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res, next) => {
    let userBalance;

    try {
        userBalance = await EcoPoolController.userBalance();
    } catch (error) {
        return res.send({ success: false });
    }

    if (!userBalance.success) {
        return res.send({ success: false });
    }

    return res.send({
        success: true,
        answer: {
            userBalance: userBalance.answer
        }
    });
};
