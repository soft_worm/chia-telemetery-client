const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');
const ChiaController = require('../../controlles/chia-controller');
const state = require('../../state');

router.post('/', async (req, res, next) => {
    const {
        body: {
            login,
            password
        }
    } = req;
    let requestData;

    try {
        requestData = await EcoPoolController.userRegistration({
            login,
            password
        });
    } catch (error) {
        return res.send(error);
    }

    const { success } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    const { answer: { token } } = requestData;
    const checkChia = await ChiaController.checkCLI();

    state.token = token;
    requestData.answer.checkChia = checkChia;

    if (checkChia) {
        // await ChiaController.init();
        return res.send(requestData);
    } else {
        return res.send(requestData);
    }
});

module.exports = router;
