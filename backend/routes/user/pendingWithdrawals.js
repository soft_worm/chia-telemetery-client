const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res) => {
    let pendingWithdrawals;
    try {
        pendingWithdrawals = await EcoPoolController.pendingWithdrawals();
    } catch (error) {
        return res.send({ success: false });
    }

    if (!pendingWithdrawals.success) {
        return res.send(pendingWithdrawals);
    }

    return res.send({
        success: true,
        answer: {
            pendingWithdrawals: pendingWithdrawals.answer
        }
    });
};
