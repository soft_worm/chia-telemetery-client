const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res, next) => {
    const {
        body: { token }
    } = req;
    const serverToken = req.cookies.token;

    if (token && serverToken !== token) {
        return res.send({ success: false });
    }

    let poolVolumeData;
    let userVolumeData;

    try {
        poolVolumeData = await EcoPoolController.poolVolume(token);
    } catch (error) {
        return res.send({ success: false });
    }

    try {
        userVolumeData = await EcoPoolController.userVolume(token);
    } catch (error) {
        return res.send({ success: false });
    }

    if (!poolVolumeData.success || !userVolumeData.success) {
        return res.send({ success: false });
    }

    return res.send({
        success: true,
        answer: {
            pool: poolVolumeData.answer.volume,
            user: userVolumeData.answer.volume
        }
    });
};
