// const bcrypt = require('bcrypt');

const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res) => {
    let userBalance;

    const {
        amount,
        xchAddress,
        password,
        timestamp
    } = req.body;

    if (isNaN(+amount)) {
        res.status(500);
        res.send({
            success: false,
            error: 'Amount is not a number!'
        });
    }

    const payload = {
        address: xchAddress,
        amount,
        timestamp
    };

    // const pwdHash = await bcrypt.hash(password, 10);

    try {
        userBalance = await EcoPoolController.makeWithdraw(payload, password);
    } catch (error) {
        return res.send({ success: false });
    }

    if (!userBalance.success) {
        return res.send(userBalance);
    }

    return res.send({
        success: true,
        answer: {
            userBalance: userBalance.answer
        }
    });
};
