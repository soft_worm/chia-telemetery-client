const express = require('express');
const router = express.Router();
const userSetChia = require('./setChia');
const userVolume = require('./volume');
const userBalance = require('./balance');
const makeWithdraw = require('./makeWithdraw');
const pendingWithdrawals = require('./pendingWithdrawals');

router.post('/set-chia', userSetChia);
router.get('/volume', userVolume);
router.get('/balance', userBalance);
router.post('/make-withdraw', makeWithdraw);
router.get('/pending-withdrawals', pendingWithdrawals);

module.exports = router;
