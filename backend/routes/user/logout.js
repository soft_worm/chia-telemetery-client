const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');

const localConfig = require('../../controlles/localConfig');
const ioClient = require('../../controlles/ecopool-controller/ioClient');

router.get('/', async (req, res) => {
    let requestData;

    try {
        requestData = await EcoPoolController.userLogout();
    } catch (error) {
        return res.send({ success: false });
    }

    localConfig.set('currentUser', undefined);
    ioClient.emit('dropUser');

    return res.send(requestData);
});

module.exports = router;
