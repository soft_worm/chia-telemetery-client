const ChiaController = require('../../controlles/chia-controller');

module.exports = async (req, res, next) => {
    const { body: { token } } = req;

    const plots = await ChiaController.getPlots(true);
    const badPlots = await ChiaController.getBadPlots();

    return res.send({
        success: true,
        answer: {
            // token:
            plots,
            badPlots
        }
    });
};
