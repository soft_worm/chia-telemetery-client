const express = require('express');
const router = express.Router();
// const createPlot = require('./createPlot');
const farm = require('./farm');

// router.post('/create-plot', createPlot);
router.get('/farm', farm);

module.exports = router;
