const express = require('express');
const router = express.Router();
const ChiaController = require('../../controlles/chia-controller');

const { Harvester } = require('chia-client');

const harvesterInstance = new Harvester();

const config = require('config');
const debug = require('debug');
const debugLog = debug(`${config.debugName}:routes:plots:log`);
const debugError = debug(`${config.debugName}:routes:plots:error`);
debugLog.log = debugError.log = console.log;

let plotsInPending = [];

router.get('/', async (req, res, next) => {
    res.render('plots',{plots: plotsInPending});
});


router.post('/add-plots', async (req, res) => {
    const {
        size,
        quantityPlots,
        ram,
        threads,
        segments,
        disablesBitfield,
        tempDirectory,
        finalDirectory
    } = req.body;

    const chiaData = await ChiaController.callCli({
        'plots': undefined,
        'create': undefined,
        '-k': Number(size), // размер плота
        '--override-k': undefined,
        '-t': `~${tempDirectory}`, // временная директория
        '-d': `~${finalDirectory}`, // финальная директория
        '-r': Number(threads), // количество потоков
        '-b': Number(ram), // RAM
        '-e': undefined, // побитовое поле по умолчанию не надо передавать
        '-n': Number(quantityPlots), // количество участков подряд
        '-u': Number(segments), // количество корзин
        '-f': "97d571995423d6c5620afcf3b8a9e29f95dcb4f5a94ee6df67cc6edebec2e17851937ed62c054783e0f1bd007400851c", // этот и следующий параметры вы конечно можете поменять, ну камон пацаны)))
        '-p': "a8f696e15a43cd45c5bf142b75de328a1931d926bd17ee11ca8c9db7d7f34cecacd1010db0c69c2f39cdde126cc05d2c" // нах тагда весь замут?) майните с оригинального по соло тогда)
    });

    plotsInPending.push({
        'size': Number(size), // размер плота
        'tempDirectory': `~${tempDirectory}`, // временная директория
        'finalDirectory': `~${finalDirectory}`, // финальная директория
        'threads': Number(threads), // количество потоков
        'ram': Number(ram), // RAM
        '-e': undefined, // побитовое поле по умолчанию не надо передавать
        'quantityPlots': Number(quantityPlots), // количество участков подряд
        'segments': Number(segments), // количество корзин
        procChild: chiaData
    });

    chiaData.on('exit', () => {
        plotsInPending = plotsInPending.filter((item) => item.procChild !== chiaData);
    });

    new Promise((resolve, reject) => {
        let dataSent = false;

        chiaData.stderr.on('data', function (data) {
            if (!dataSent) {
                dataSent = true;
                reject(data.toString().trim());
            }
        });
        chiaData.stdout.on('data', function (data) {
            if (!dataSent) {
                dataSent = true;
                resolve(true);
            }
        });
    })
        .then(() => {
            res.json({
                success: true,
                answer: true
            })
        })
        .catch((err) => {
            res.json({
                success: false,
                err
            });
        });

})
module.exports = router;
