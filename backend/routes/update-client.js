const axios = require('axios');

const config = require('config');
const exec = require('child_process').exec;
const express = require('express');
const router = express.Router();

const debug = require('debug');
const debugLog = debug(`${config.debugName}:routes:farms:log`);
const debugError = debug(`${config.debugName}:routes:farms:error`);
debugLog.log = debugError.log = console.log;

router.get('/version', async (req, res) => {
    axios({
        method: 'get',
        url: 'https://gitlab.com/ecopool-public/chia-telemetery-client/-/raw/master/package.json'
    })
        .then((result) => {
            res.send(result.data.version);
        })
});

router.get('/', async (req, res) => {
    await new Promise((resolve) => {
        exec('git pull', {
            cwd: process.cwd()
        }, function (error, stdout, stderr) {
            debugLog(stdout);
            if (stderr) {
                debugError(stderr)
            }
            resolve();
        });
    });

    res.send();

    exec('pm2 restart all', {
        detached: true
    }, function(error, stdout, stderr) {
        console.log(stdout)
    });
});

module.exports = router;
