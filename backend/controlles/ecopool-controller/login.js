const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const setCookie = require('set-cookie-parser');

const config = require('config');
const localConfig = require('../localConfig');
const ioClient = require('../ecopool-controller/ioClient');

module.exports = async (payload) => {
    const {
        login,
        password,
        token
    } = payload;
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.login,
            data: {
                login,
                password
            },
            token
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;
    const parseCookies = setCookie.parse(requestData.headers['set-cookie']);
    const cookieSid = parseCookies.find(cookie => {
        return cookie.name === 'connect.sid';
    });

    if (status !== 200) {
        return { success: false };
    }

    if (!success) {
        return {
            success: false,
            answer: requestData.data.answer
        };
    }

    localConfig.set('currentUser', login);
    ioClient.emit('setupUser', config.currentUser, ({success}) => {
        console.log('setupUser', success);
    });

    return {
        success: true,
        answer: {
            // token: cookieSid.value
        }
    };
};
