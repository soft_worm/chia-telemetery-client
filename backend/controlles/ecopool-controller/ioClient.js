const config = require('config');
const IOClient = require('socket.io-client');
const io = IOClient(config.poolData.host);

io.on('connect', () => {
    io.emit('setupUser', config.currentUser, ({success}) => {
        console.log('setupUser', success);
    });
});

module.exports = io;
