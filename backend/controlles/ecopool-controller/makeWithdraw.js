const ioClient = require('./ioClient');

module.exports = async (payload, password) => {
    let requestData;

    try {
        requestData = await new Promise((resolve) => {
            ioClient.emit('create-withdrawal-request', payload, password, resolve);
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    if (!requestData.success) {
        return requestData;
    }

    return {
        success: true,
        answer: requestData
    };
};
