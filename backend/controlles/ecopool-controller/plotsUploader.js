const getPlots = require('../chia-controller/getPlots');
const addPlots = require('./addPlots');

const sendPlots = async (token) => {
    const allPlots = await getPlots();
    addPlots(allPlots, token)
        .then(resp => {
            // console.log(resp);
        })
        .catch(error => {
            // console.error(error);
        });
};

let isStarted = false;
let timeout = 1000 * 60 * 5; // 5 минут
let timer = null;

module.exports = {
    start: (token) => {
        if (isStarted) {
            return false;
        }

        sendPlots(token);
        timer = setInterval(() => {
            sendPlots(token);
        }, timeout);
        isStarted = true;
    },
    stop: () => {
        clearInterval(timer);
        isStarted = false;
    }
};
