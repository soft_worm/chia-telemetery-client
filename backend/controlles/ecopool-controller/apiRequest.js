const axios = require('axios');
const config = require('config');
const state = require('../../state');

require('./ioClient');

module.exports = (payload) => {
    const {
        url,
        method = 'post',
        data = {}
    } = payload;
    const hasToken = Boolean(state.token);

    return axios({
        method,
        url: `${config.poolData.host}${url}`,
        data,
        headers: hasToken ? {
            Cookie: `connect.sid=${state.token}`
        } : {}
    });
};
