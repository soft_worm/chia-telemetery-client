const ioClient = require('./ioClient');

module.exports = async () => {
    let requestData;

    try {
        requestData = await new Promise((resolve) => {
            ioClient.emit('pool-volume', resolve);
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    return {
        success: true,
        answer: requestData
    };
};
