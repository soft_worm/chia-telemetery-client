const ECO_POOL_API = require('../../constants/EcoPoolApi');
// const apiRequest = require('./apiRequest');
const state = require('../../state');
const ioClient = require('./ioClient');

module.exports = async (plots) => {
    try {
        // error 413 fix - request body too large
        const chunkSize = 10;
        for (let i=0, length = plots.length; i<length; i+=chunkSize) {
            const currentChunk = plots.slice(i,i+chunkSize);

            ioClient.emit('add-plots', currentChunk);
        }
    } catch (error) {
        throw { success: false };
    }

    if (status !== 200) {
        throw { success: false };
    }

    return { success: true };
};
