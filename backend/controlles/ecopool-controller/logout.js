const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');

module.exports = async () => {
    let requestData;

    try {
        requestData = await apiRequest({
            method: 'get',
            url: ECO_POOL_API.logout
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;

    if (status !== 200 || !success) {
        return { success: false };
    }

    return { success: true };
};
