const fs = require('fs');

const buf2hex = buffer => {
    return [...new Uint8Array(buffer)].map(x => {
        return x.toString(16).padStart(2, '0');
    }).join('');
};

module.exports = (fileName) => {
    const fileStream = fs.createReadStream(fileName);

    return new Promise(resolve => {
        fileStream.on('data', (chunk) => {
            const memoSize = +('0x' + buf2hex(chunk.slice(58, 60)));
            const memoHex = buf2hex(chunk.slice(60, 60 + memoSize));
            fileStream.close();
            resolve(memoHex);
        });
    });
};
