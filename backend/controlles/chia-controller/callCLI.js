const {spawn} = require('child_process');
const config = require('config');
const debug = require('debug');
const debugLog = debug(`${config.debugName}:call-cli`);
const discoverCLI = require('./discoverCLI');
const fs = require('fs');

debugLog.log = console.log;

module.exports = (payload = {}, customCLIPath = null) => {
    const cliPath = customCLIPath ? customCLIPath : discoverCLI();

    if (!cliPath) {
        throw new Error('No Chia CLI installed!');
    }

    const payloadForCommand = Object.keys(payload)
        .reduce((acc, key) => {
            acc.push(key);

            if (payload[key]) {
                acc.push(payload[key]);
            }

            return acc;
        }, []);

    let child;

    if (process.platform === 'win32') {
        child = spawn(`${cliPath} ${payloadForCommand.join(' ')}`, {
            shell: true,
            detached: true,
            windowsHide: true
        });
    } else {
        if (fs.existsSync(`${cliPath}/activate`)) {
            const command = `. ${cliPath}/activate && chia ${payloadForCommand.join(' ')}`;
            child = spawn(command, {
                shell: true,
                detached: true
            });
        } else {
            const command = `${cliPath}/chia ${payloadForCommand.join(' ')}`;
            child = spawn(command, {
                shell: true,
                detached: true
            });
        }
    }

    const pid = child.pid;
    const pidLog = debug(`${config.debugName}:call-cli:${pid}`);
    pidLog.log = console.log;

    const pidLogErr = debug(`${config.debugName}:call-cli:${pid}`);

    child.stderr.on('data', (data) => {
        // console.log(data.toString().trim());
        pidLogErr(data.toString().trim());
    });
    child.stdout.on('data', (data) => {
        // console.log(data.toString().trim());
        pidLog(data.toString().trim()
        );
    });
    child.on('exit', (exitCode) => {
        // console.log(exitCode);
        // console.log(ex);
        pidLog("Child exited with code: " + exitCode);
    });

    return child;
};
