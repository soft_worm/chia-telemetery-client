const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'configure': null,
        '--enable-upnp': null,
        'false': null
    });

    return checkCommand.status;
};
