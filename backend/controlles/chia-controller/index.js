const discoverCLI = require('./discoverCLI');
const callCLI = require('./callCLI');
const callCLIPromise = require('./callCLIPromise');
const checkCLI = require('./checkCLI');
const setCLIPath = require('./setCLIPath');
const stopAllProcess = require('./stopAllProcess');
const setCertificates = require('./setCertificates');
const setUpnp = require('./setUpnp');
const setFarmerPeer = require('./setFarmerPeer');
const setLogToDebug = require('./setLogToDebug');
const startHarvester = require('./startHarvester');
const createTestPlot = require('./createTestPlot');
const createPlot = require('./createPlot');
const getPlotMemo = require('./getPlotMemo');
const getPlots = require('./getPlots');
const getBadPlots = require('./getBadPlots');
const refreshPlots = require('./refreshPlots');
const addPlotsDirectory = require('./addPlotsDirectory');
const init = require('./init');

module.exports = {
    discoverCLI,
    callCLI,
    callCLIPromise,
    checkCLI,
    setCLIPath,
    init,
    stopAllProcess,
    startHarvester,
    setCertificates,
    setUpnp,
    setFarmerPeer,
    setLogToDebug,
    createTestPlot,
    createPlot,
    getPlotMemo,
    getPlots,
    getBadPlots,
    refreshPlots,
    addPlotsDirectory
};
