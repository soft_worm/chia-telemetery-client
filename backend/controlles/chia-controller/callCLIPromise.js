const callCLI = require('./callCLI');

module.exports = (payload, customCLIPath) => {
    return new Promise(resolve => {
        const child = callCLI(payload, customCLIPath);

        child.on('exit', function (exitCode) {
            resolve({
                exitCode,
                status: exitCode === 0
            });
        });
    });
};
