const { Harvester } = require('chia-client');
const HarvesterInstance = new Harvester();

module.exports = async () => {
    await HarvesterInstance.refreshPlots();
    const plots = await HarvesterInstance.getPlots();

    if (!plots.success) {
        return [];
    }

    const badPlots = plots.not_found_filenames;

    if (!badPlots.length) {
        return [];
    }

    return badPlots;
};
