const localConfigController = require('../localConfig');

module.exports = (newPath) => {
    localConfigController.set('chiaCliPath', newPath);
};
