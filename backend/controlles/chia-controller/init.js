const stopAllProcess = require('./stopAllProcess');
const setCertificates = require('./setCertificates');
const setUpnp = require('./setUpnp');
const setFarmerPeer = require('./setFarmerPeer');
const setLogToDebug = require('./setLogToDebug');
const startHarvester = require('./startHarvester');
const EcoPoolController = require('../ecopool-controller/index');
const state = require('../../state');

module.exports = async () => {
    setTimeout(() => {
        EcoPoolController.plotsUploader.start(state.token);
    }, 5000);

    await stopAllProcess();
    const statusSetCertificates = await setCertificates();

    if (!statusSetCertificates) {
        return false;
    }

    const statusSetUpnp = await setUpnp();

    if (!statusSetUpnp) {
        return false;
    }

    const statusSetFarmerPeer = await setFarmerPeer();

    if (!statusSetFarmerPeer) {
        return false;
    }

    const statusSetLogToDebug = await setLogToDebug();

    if (!statusSetLogToDebug) {
        return false;
    }

    const statusStartHarvester = await startHarvester();

    if (!statusStartHarvester) {
        return false;
    }

    return true;
};
