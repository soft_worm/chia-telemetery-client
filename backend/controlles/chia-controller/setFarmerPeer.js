const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'configure': null,
        '--set-farmer-peer': null,
        '136.243.104.155:8447': null
    });

    return checkCommand.status;
};
