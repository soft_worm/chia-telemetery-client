const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'stop': null,
        'all': null
    });

    return checkCommand.status;
};
