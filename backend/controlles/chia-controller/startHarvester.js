const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        start: null,
        harvester: null,
        '-r': null
    });

    return checkCommand.status;
};
