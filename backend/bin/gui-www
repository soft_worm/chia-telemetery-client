#!/usr/bin/env node

/**
 * Module dependencies.
 */

const express = require('express');
const cookieParser = require('cookie-parser');
const path = require('path');
const fs = require('fs');

const app = express();
// const chiaController = require('./controlles/chia-controller');

const indexContent = fs.readFileSync(path.resolve(process.cwd(), 'dist', 'index.html'));
app.use('/', (req, res, next) => {
    if (['/dashboard', '/farm', '/'].indexOf(req.path) !== -1) {
        res.set('Content-Type', 'text/html');
        res.send(indexContent);
    } else {
        next();
    }
});

app.use(
    '/', express.static(path.resolve(process.cwd(), 'dist'))
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

module.exports = app;

const debugLog = require('debug')('ecopool-client:start');
debugLog.log = console.log;
const http = require('http');
const config = require('config');

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(config.client.port || 3401);
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, `0.0.0.0`);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Your server started on http://localhost:3401');
    debugLog('Listening on ' + bind);
}

// (async () => {
//     //  start of harvester
//     const cliController = require('../controlles/chia-controller');
//
//     // stop all services
//     await cliController.callCliPromise({
//         'stop': void 0,
//         'all': void 0,
//         '-d': void 0
//     });
//
//     //  start only harvester
//     await cliController.callCliPromise({
//         'start': void 0,
//         'harvester': void 0,
//         '-r': void 0
//     });
// })();
