const camelCase = require('camelcase');
const getOnlyFileName = require('./getOnlyFileName');

module.exports = (rawObject) => {
    const objectKeys = Object.keys(rawObject);

    return objectKeys.reduce((acc, key) => {
        if (key === 'filename') {
            acc[key] = getOnlyFileName(rawObject[key]);
        } else {
            acc[camelCase(key)] = rawObject[key];
        }

        return acc;
    }, {});
};
