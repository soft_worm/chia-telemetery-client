module.exports = (req, cookieName) => {
    return req && req.cookies ? req.cookies[cookieName] : null;
}
